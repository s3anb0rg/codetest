﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameThrower : MonoBehaviour
{
    private void OnParticleCollision(GameObject other)
    {
        if (other != gameObject.transform.parent.gameObject)
            other.GetComponent<BoxCollider>().SendMessage("TakeDamage", 5f, SendMessageOptions.DontRequireReceiver);
    }
}
