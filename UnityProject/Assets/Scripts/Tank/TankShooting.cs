﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public enum Weapons
{
    Rocket,
    flamethrower
}

public class TankShooting : MonoBehaviour
{
    public Weapons SelectedWeapon = Weapons.Rocket;         // Default selected weapon type
    public float m_TotalRocketAmmo = 10f;                   // The amount of rocket ammo the tank has              
    public float m_TotalFlameThrowerAmmo = 100f;            // The amount of flamethrower ammo the tank has     
    public ParticleSystem m_FlameThrowerPS;                 // The particle system component for the flamethrower
    public Slider m_AmmoSlider;                             // The Amount of ammo each tank starts off with.
    public Image m_FillImage;                               // The image component of the slider.
    public Color m_FullRocketAmmoColor = Color.blue;        // The color the Rocket Launcher will be when full.
    public Color m_FullFlameAmmoColor = Color.red;          // The color the Flame thrower will be when full.
    public Color m_ZeroAmmoColor = Color.black;             // The color the health bar will be when on no health.
    public Rigidbody m_Shell;                               // Prefab of the shell.
    public Transform m_FireTransform;                       // A child of the tank where the shells are spawned. 
    public Slider m_AimSlider;                              // A child of the tank that displays the current launch force.
    public AudioSource m_ShootingAudio;                     // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
    public AudioClip m_ChargingClip;                        // Audio that plays when each shot is charging up.
    public AudioClip m_FireClip;                            // Audio that plays when each shot is fired.
    public float m_MinLaunchForce = 15f;                    // The force given to the shell if the fire button is not held.
    public float m_MaxLaunchForce = 30f;                    // The force given to the shell if the fire button is held for the max charge time.
    public float m_MaxChargeTime = 0.75f;                   // How long the shell can charge for before it is fired at max force.

    private float m_CurrentRocketAmmo;                      // How much ammo the tank currently has.
    private float m_CurrentFlameThrowerAmmo;                // How much ammo the tank currently has.
    private float m_CurrentLaunchForce;                     // The force that will be given to the shell when the fire button is released.
    private float m_ChargeSpeed;                            // How fast the launch force increases, based on the max charge time.
    private bool m_ChargingRocket;
    private bool m_IsFlameThrowerCooledDown;

	public bool IsCharging
	{
		get { return m_ChargingRocket; }
	}

    private void OnEnable()
    {
        // When the tank is turned on, reset the launch force, ammo, and the UI
        m_CurrentLaunchForce        = m_MinLaunchForce;
        m_CurrentRocketAmmo         = m_TotalRocketAmmo;
        m_IsFlameThrowerCooledDown  = true;
        m_CurrentFlameThrowerAmmo   = m_TotalFlameThrowerAmmo;
        m_AimSlider.value           = m_MinLaunchForce;

        SetWeaponAmmoUI();
    }

    private void Start ()
    {
        // The rate that the launch force charges up is the range of possible forces by the max charge time.
        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
    }

	public void BeginChargingShot()
	{
        if (SelectedWeapon == Weapons.Rocket)
        {
            if (m_ChargingRocket) return;

            //you can only fire this weapon if you have ammo left
            if (m_CurrentRocketAmmo > 0)
            {
                m_CurrentLaunchForce = m_MinLaunchForce;

                // Change the clip to the charging clip and start it playing.
                m_ShootingAudio.clip = m_ChargingClip;
                m_ShootingAudio.Play();

                m_ChargingRocket = true;
            }
            else
            {
                CycleWeapon();
            }
        }

        else if (SelectedWeapon == Weapons.flamethrower)
        {
            if (m_CurrentFlameThrowerAmmo > 0)
            {
                FireFlameThrower();
            }
            else
            {
                if (m_FlameThrowerPS.isPlaying)
                    m_FlameThrowerPS.Stop();
            }
        }
    }

    public void FireChargedShot()
	{
        if (SelectedWeapon == Weapons.Rocket)
        {
            if (!m_ChargingRocket) return;

            FireRocket();
            m_ChargingRocket = false;
        }
        else if (SelectedWeapon == Weapons.flamethrower)
        {
            m_FlameThrowerPS.Stop();
            //start flamethrower cooldown
            m_IsFlameThrowerCooledDown = false;
            StartCoroutine(FlameThrowerCoolDown());
        }
    }

    public void RefillAmmo()
    {
        m_CurrentFlameThrowerAmmo = m_TotalFlameThrowerAmmo;
        m_CurrentRocketAmmo = m_TotalRocketAmmo;
        SetWeaponAmmoUI();
    }

    private void Update()
	{
		if (m_ChargingRocket && SelectedWeapon == Weapons.Rocket)
		{
			m_CurrentLaunchForce = Mathf.Min(m_MaxLaunchForce, m_CurrentLaunchForce + m_ChargeSpeed*Time.deltaTime);
			m_AimSlider.value = m_CurrentLaunchForce;
		}
		else
		{
			m_AimSlider.value = m_MinLaunchForce;
		}
    }

    public void CycleWeapon()
    {
        if (SelectedWeapon == Weapons.Rocket)
        {
            SelectedWeapon = Weapons.flamethrower;
            SwapWeaponUI(m_TotalFlameThrowerAmmo);
        }
        else if (SelectedWeapon == Weapons.flamethrower)
        {
            SelectedWeapon = Weapons.Rocket;
            SwapWeaponUI(m_TotalRocketAmmo);
        }
    }

    private void SwapWeaponUI(float totalSelectedWeaponAmmo)
    {
        m_AmmoSlider.maxValue = totalSelectedWeaponAmmo;
        SetWeaponAmmoUI();
    }

    private void SetWeaponAmmoUI()
    {
        if (SelectedWeapon == Weapons.Rocket)
        {
            // Set the slider's value appropriately.
            m_AmmoSlider.value = m_CurrentRocketAmmo;

            // Interpolate the color of the bar between the choosen colours based on the current percentage of the starting health.
            m_FillImage.color = Color.Lerp(m_ZeroAmmoColor, m_FullRocketAmmoColor, m_CurrentRocketAmmo / m_TotalRocketAmmo);
        }
        if (SelectedWeapon == Weapons.flamethrower)
        {
            // Set the slider's value appropriately.
            m_AmmoSlider.value = m_CurrentFlameThrowerAmmo;

            // Interpolate the color of the bar between the choosen colours based on the current percentage of the starting health.
            m_FillImage.color = Color.Lerp(m_ZeroAmmoColor, m_FullFlameAmmoColor, m_CurrentFlameThrowerAmmo / m_TotalFlameThrowerAmmo);
        }
    }

    private void FireRocket()
    {
        //Decrement the amount of ammo the tank has
        m_CurrentRocketAmmo -= 1;
        SetWeaponAmmoUI();

        // Create an instance of the shell and store a reference to it's rigidbody.
        Rigidbody shellInstance =
            Instantiate (m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

        // Set the shell's velocity to the launch force in the fire position's forward direction.
        shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward; 

        // Change the clip to the firing clip and play it.
        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play ();

        // Reset the launch force.  This is a precaution in case of missing button events.
        m_CurrentLaunchForce = m_MinLaunchForce;
    }

    private void FireFlameThrower()
    {
        if (m_IsFlameThrowerCooledDown)
        {
            if (!m_FlameThrowerPS.isPlaying)
            {
                m_FlameThrowerPS.Clear();
                m_FlameThrowerPS.Play();
            }
            m_CurrentFlameThrowerAmmo -= 0.2f;
            SetWeaponAmmoUI();
        }
    }

    private IEnumerator FlameThrowerCoolDown()
    {
        yield return new WaitForSeconds(m_FlameThrowerPS.main.startLifetime.constant);
        m_IsFlameThrowerCooledDown = true;
    }
}