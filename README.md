## Sean Borg Hyper Luminal Code Test ##
## Repository: https://bitbucket.org/s3anb0rg/codetest/src/master/ ##
## Changes Made ##
1. Tanks have Finite ammo. The amount of ammo they have left is indicated by the outer UI ring around the tank.
2. There is a new flamethrower weapon in the game. Player 1 can cycle between weapons with buttons Q and E. Player 2 can cycle between weapons with buttons 1 and 3. The ammo UI wheel is an indicator to show what weapon is currently equipped. If the ammo wheel is blue then the rocket weapon is equipped, whereas if the wheel is red then the flamethrower is equipped.
3. The flame thrower deals damage to enemy tanks but does not deal damage to the tank that uses the weapon. This was done because firing the flame thrower while driving forward will result in a self kill.
4. Supply crates spawn in the scene at intervals of 6 - 12 seconds. When a tank hits a supply crate, that tanks ammo fully refills.
Further Work
1. An improvement to the game would be to implement A* pathfinding or Navmesh (both of which I have experience using) to the enemy tanks to make them smarter. This can be used to target the player better and to look for supply crates when the enemy tanks run out of ammo.
2. With more time I can work on implementing this AI.
3. A score system and high score board could be put into the game, saving High score wins
locally on the machine the game is played on, and potentially online to have a global high
score board too.
4. I would organise the weapons code. The game started off with just 1 weapon. I added in
another weapon and to do this I created an enum of weapon types. With more time I would change this to just have a weapon class, being inherited by weapon subclasses. I would create different weapon types like this
Timescale
Commit 1: working on the finite supply tank functionality and UI took about an hour. The code was there for the health bar I just had to copy and change it slightly to use it for the ammo.
Commit 2: Most of the code for a new ammo type was implemented here. It took about an hour and a half. The player could now switch between weapons and doing so updates the UI
Commit 3: Worked on making the particle system for the flame thrower look better, and actually collide with tanks to deal damage to them. This took about 2 and a half hours of fiddling with particle system settings, and checking tutorials to figure out how to get the particle system to detect collisions.
Commit 4: This took about 2 minutes. All I did is make the flamethrower particle system ignore collisions with the tank that fired the weapon - so that tanks don’t self destruct when driving forward and using the flamethrower.
Commit 5: About 20 minutes. Just went through the code to clean up some redundant code that I noticed I implemented.
 
Commit 6: This took about an hour or so, I added a crate to spawn in randomly that when hit by a tank it gets deleted and that tank gets full ammo.
## TOTAL TIME TAKEN: 5-6 HOURS ISH ##